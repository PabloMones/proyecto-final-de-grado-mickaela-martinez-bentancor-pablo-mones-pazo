# Proyecto final de grado Mickaela Martinez Bentancor - Pablo Mones Pazo

El proyecto incluye el código y los datos utilizados para la realización del proyecto: "Evolutividad en la estructura de precios de la economía uruguaya. Una visión desde las redes y árboles de expansión mínima".

Los archivos excel contienen los datos requeridos. En tanto, en el archivo "Arbol.R" se encuentra el código utilizado para la obtención de los resultados correspondientes a árboles de expansión mínima mientras que "Redes.R" contiene el código correspondiente a análisis de redes. 
